<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

defined('DATE_FORMAT') or define('DATE_FORMAT','Y/m/d H:i:s');

/**
 * @param int $t
 * @return int
 */
function genie_caches_autoclean_dist (int $t): int {
	if (!defined('_CACHE_NAMESPACE')) {
		memoization();
	}
	$unused = '';
	$cleaned = [];
	$size = 0; 
	if (!function_exists('apcu_cache_info')) {
		return 1;
	}
	$f_cache_autoclean = charger_fonction('cache_autoclean', 'inc');
	$cache_info = apcu_cache_info();
	foreach ( $cache_info['cache_list'] as $entry) {
		$key = $entry['info'];
		if (!$key or (strpos($key, _CACHE_NAMESPACE) !== 0)) {
			continue;
		}
		if (($vieux = cache_est_perime($key, $unused, $entry))
			or ($f_cache_autoclean and $f_cache_autoclean($key, $entry))
		) {
			// Trace des squelettes libérés par l'autoclean
			// On ajoute un @ au début du nom du squelette lorsque c'est la fonction $f_cache_autoclean définie par le site
			// qui décide de l'invalidation et non la simple péremption du cache
			if (strpos ($key, '-', strlen (_CACHE_NAMESPACE))) {
				$start = strpos ($key, '-', strlen (_CACHE_NAMESPACE)) + 1;
			}
			else {
				$start = strlen (_CACHE_NAMESPACE);
			}
			$skel = ($vieux ? '' : '@').substr($key, $start);
			$cleaned[$skel] = ($cleaned[$skel] ?? 0) +1;
			$size += $entry['mem_size'];
			apcu_delete($key);
		}
	}
	$size = round($size/1024, 1);
	$size = (($size > 1024) ? round($size/1024, 1) . ' Mo' : $size . ' ko');
	spip_log (($n=count($cleaned))
					? ("$size libérés en supprimant $n caches périmés ou indésirés : ".str_replace(['[', '] ', '> ', "\n", "\t", '   ', 'Array(', ')'], '', print_r($cleaned, 1)))
					: 'ZERO caches supprimés',
			  'caches_autoclean'._LOG_INFO  );
	return 1;
}

if (!function_exists('cache_est_perime')) {
	// Fonctions également définies par XRay
	
	// Signature moche requise par le cadre d'usage dans XRay... à revoir   
	function cache_est_perime (string $nomcache, &$spip_cache_unused, &$apcu_entry): bool {
		return (bool) cache_est_perime_explic ($nomcache, $apcu_entry);
	}
	function cache_est_perime_explic (string $nomcache, &$data=''): array {
		global $meta_derniere_modif;
		static $time = 0;
		if (!apcu_exists($nomcache)) {
			return ['N’existe plus pour APCu', 'Ça suffit !'];
		}
		$time = ($time ?: time());
		$explic_meta = 'meta_dernière_modif='.date(DATE_FORMAT, $meta_derniere_modif);
		if ($data['ttl'] and ($data['creation_time'] + $data['ttl'] <= $time)) {
			$explic_apcu = 'Péremption APCU='.date(DATE_FORMAT,$data['creation_time'] + $data['ttl']);
			if ($data['creation_time'] < $meta_derniere_modif) {
				return [
					trim("Périmé / APCu et SPIP", ' +'),
					"$explic_apcu ; $explic_meta"
				];
			}
			return [
				trim("Périmé / APCu"),
				$explic_apcu
			];
		}
		if ($data['creation_time'] <  $meta_derniere_modif) {
			return ['Périmé / SPIP', $explic_meta];
		}
		return [];
	}
}

