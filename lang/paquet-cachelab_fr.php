<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/switchcase/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// X
	'cachelab_nom' => 'cacheLab',
	'cachelab_slogan' => 'Invalider une sélection de caches SPIP avec memoization pour APC',
	'xray_description' => 'CacheLab permet de filtrer les caches selon certains critères choisis par le programmeur (session courante, chemin du squelette, présence d’un objet dans le contexte, ou autres) et d’invalider ces caches. Lors d’une modification des données, l’invalidation totale du cache SPIP (par défaut) peut être remplacée par une invalidation sélective des seuls caches qui le nécessitent vraiment.',
);
