<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
include_spip('inc/cachelab_utils');

// Loger les appels à cache_cibler
defined('LOG_CACHELAB_CIBLER') or define ('LOG_CACHELAB_CIBLER', false);

// Loger le détail des cleans automatiques
defined('LOG_CACHELAB_CLEAN') or define ('LOG_CACHELAB_CLEAN', false);

// Détailler ou pas, par conditions, les caches atteints par cachelab_cibler  
defined('LOG_CACHELAB_CONDITIONS') or define ('LOG_CACHELAB_CONDITIONS', false);
// Mais pas les conditions exigeant cette valeur de 'chemin' car yen aurait trop et c'est pas intéressant pour l'instant :
defined('LOG_CACHELAB_CONDITIONS_EXCEPTION') or define ('LOG_CACHELAB_CONDITIONS_EXCEPTION', ''); 

// Ne pas faire deux fois le même del (mêmes conditions, mêmes options) dans le même hit
defined('CACHELAB_EVITER_DOUBLON_DEL') or define('CACHELAB_EVITER_DOUBLON_DEL', true);

/**
 *
 * Applique une action sur un cache donné et renvoie éventuellement une donnée
 * Nécessite Mémoization (toutes méthodes OK).
 *
 * @param string $action    del, pass, list, clean, list_html, get, get_html ou user defined
 * @param string $cle       clé du cache ciblé
 * @param null|array $data  valeur du cache pour cette clé (pas forcément fourni)
 * @param array $options    options pour les fonctions cachelab_cibler_{action}
 * @param array|string|null &$return          
 *                          array renvoyé par les actions `list` ou `get`, 
 *                          ou string a priori pour `get_html`
 * @return bool             indique si l'action a pu être appliquée ou non
 */
function cachelab_appliquer(string $action, string $cle, ?array $data = null, array $options = [], &$return = null) : bool {
	$Memoization = memoization();

static $len_prefix;
	if (!$len_prefix) {
		$len_prefix = strlen(_CACHE_NAMESPACE);
	}
	$joliecle = substr($cle, $len_prefix);

	switch ($action) {
	case 'del':
		$del = $Memoization->del($joliecle);
		if (!$del) {
			cachelab_log("Échec 'del' $joliecle", 'ERREUR_cachelab'._LOG_ERREUR);
			return false;
		}
		break;

	// entièrement gérés par cachelab_cibler
	case 'pass':	// passe
	case 'list':	// renvoie les clés
	case 'clean':	// nettoie
		break;
		
	case 'list_html':	// renvoie les contenus indexés par les clés sans préfixes
						// attention ça peut grossir !
		if (!is_array($return)) {
			$return = array();
		}
		$return[$joliecle] = $data['texte'];
		break;

	case 'get':	// renvoie le 1er cache ciblé
		if (!$data) {
			$data = $Memoization->get($joliecle);
		}
		$return = $data;
		break;

	case 'get_html':	// renvoie le contenu du 1er cache ciblé
		if (!$data) {
			$data = $Memoization->get($joliecle);
		}
		$return = ($data['texte'] ?? '');
		break;

	default:
		$f = 'cachelab_appliquer_'.$action;
		if (function_exists($f)) {
			return $f($action, $cle, $data, $options, $return);
		}
		cachelab_log("L'action '$action' n'est pas définie pour cachelab_applique", 'ERREUR_cachelab'._LOG_ERREUR);
		return false;
	}
	return true;
}

/**
 * Extrait du tableau les différentes conditions précodées
 * @param array $conditions
 * @return ?array null ou [session, chemin, cle_objet, id_objet, plusfunc, plus_args]
 */
function cachelab_prepare_conditions(array $conditions): ?array {
	// filtrage
	$session = ($conditions['session'] ?? null);
	if ($session==='courante') {
		$session = spip_session();
	}

	$chemin = ($conditions['chemin'] ?? null);

	$cle_objet = ($conditions['cle_objet'] ?? null);
	$id_objet = ($conditions['id_objet'] ?? null);
	if ($cle_objet and !$id_objet) {
		cachelab_log("cachelab_cibler : $cle_objet inconnu càd bad conditions=".print_r($conditions, 1)."\nPile=\n".print_r(debug_backtrace(), 1), 'ERREUR_cachelab'._LOG_ERREUR);
		$cle_objet=null;
	}

	$plus_args=[];
	// pour 'contexte' on utilise un 'plus' juste pour donner un exemple d'extension
	if (isset($conditions['contexte']) and $conditions['contexte'] and !isset($conditions['plus'])) {
		$conditions['plus'] = 'contexte';
		$plus_args = $conditions['contexte'];
	}

	$plus = (isset($conditions['plus']) ? (string)$conditions['plus'] : '');
	if ($plus) {
		$plusfunc='cachelab_ciblercache_'.$plus;
		// Signature nécessaire : $plusfunc ($action, $conditions, $options, &$stats)
		if (!function_exists($plusfunc)) {
			cachelab_log("La fonction '$plusfunc' n'est pas définie", 'ERREUR_cachelab'._LOG_ERREUR);
			return null;
		}
		if(!$plus_args and isset($conditions['plus_args'])) {
			$plus_args = $conditions['plus_args'];
		}
	} else {
		$plusfunc = '';
	}
	return [
		'session' => $session,
		'chemin' => $chemin,
		'cle_objet' => $cle_objet,
		'id_objet' => $id_objet,
		'plusfunc' => $plusfunc,
		'plus_args' => $plus_args
	];
}

/**
 *
 * Applique une action donnée à tous les caches vérifiant certaines conditions
 *
 * @uses apcu_cache_info() et donc nécessite que Memoization soit activé avec APC ou APCu
 *
 * @param string $action    l'action à appliquer : del, pass, list, clean, list_html, get, get_html ou user defined
 * @param array $conditions les conditions définissant la cible
 * @param array $options    options de l'action et/ou des conditions
 * @return array|null
 *      le résultat si c'est une action 'get' ou 'get_...'
 *      la liste des stats sinon,
 *      avec éventuellement la liste des résultats, pour action 'list' ou 'list_html' ou option 'list'
 *
 */
function cachelab_cibler(string $action, array $conditions = array(), array $options = []): ?array {
	$Memoization = memoization();
	if (!in_array($Memoization->methode(), array('apc', 'apcu'))) {
		cachelab_log($m="cachelab_cibler($action...) : Mémoization n'est pas activé avec APC ou APCu", 'ERREUR_cachelab'._LOG_ERREUR);
		die($m);
	}
	$return = $c_session = $c_chemin = $c_cle_objet = $c_id_objet = $c_plusfunc = null;
	if (LOG_CACHELAB_CIBLER) {
		cachelab_log("cachelab_cibler ".print_r(['action'=>$action, 'conditions'=>$conditions, 'options'=>$options], 1), 'cachelab');
	}
	
	// Prise en compte des 'ou' (ou 'or', alternatives de conditions)
	if ($conditions['ou'] ?? false) {
		$l_conditions = $conditions['ou'];
	}
	elseif ($conditions['or'] ?? false) {
		$l_conditions = $conditions['or'];
	}
	else {
		$l_conditions = [ $conditions ];
	}
	if (!is_array($l_conditions)) {
		cachelab_log ("La condition OU ou OR pour cachelab_cibler($action,...) n'est pas un tableau : " . print_r ($conditions, 1), 'ERREUR_cachelab'._LOG_ERREUR);
		return null;
	}

	// $l_conditions est un tableau de conditions élémentaires
	// La condition globale est satisfaite si l'une des condition est satisfaite (OR)
	// Chaque condition élémentaire est satisfaite si chacune de ses composantes est satisfaite (AND)
	//
	// On pourrait mieux préparer en sortant les explode de la boucle sur les caches
	// mais bof atm ça gagne moins de 3 ms pour 10000 caches
	$l_conditions = array_map ('cachelab_prepare_conditions', $l_conditions);
	// prend la forme d'un tableau de conditions précalculées [$session, $chemin, $cle_objet, $id_objet, $plusfunc]
	if ((_request('exec') === 'xray') and (_request('debug') === 'cachelab')) {
		echo "<h3>Les conditions préparées</h3><xmp>".print_r($l_conditions,1)."</xmp>";
	}

	// options
	// explode+strpos par défaut pour les chemins
	$c_methode_chemin = ($options['methode_chemin'] ?? 'strpos');
	$c_partie_chemin = ($options['partie_chemin'] ?? 'tout');
	// clean par défaut
	$do_clean = ($options['clean'] ?? (!defined('CACHELAB_CLEAN') or CACHELAB_CLEAN));
	// pas de listes par défaut
	$do_lists = (($action === 'list') or (isset($options['list']) and $options['list']));
	include_spip('lib/microtime.inc');
	microtime_do('begin');

	// retours
	$stats = [];
	$stats['nb_alien'] = $stats['nb_candidats'] = $stats['nb_clean'] = $stats['nb_cible'] = 0;
	$stats['l_cible'] = [];

	// On y va
	$cache = apcu_cache_info();
	$meta_derniere_modif = $GLOBALS['meta']['derniere_modif'];
	$len_prefix = strlen(_CACHE_NAMESPACE);

	$n_condition =0;
	if (LOG_CACHELAB_CONDITIONS
			and (!LOG_CACHELAB_CONDITIONS_EXCEPTION
				or  (($i_conditions['chemin'] ?? '') !== LOG_CACHELAB_CONDITIONS_EXCEPTION))) {
		foreach ($l_conditions as $i_conditions) {
			$n_condition++;
			$m = "\n====\n====\nCondition $n_condition = " . print_r ($i_conditions, 1);
			$f = 'cachelab_cibler_cond_' . $n_condition . '_DEBUG';
			cachelab_log ($m, $f);
		}
	}

	static $appels_meme_hit = [];
	ob_start ();
	debug_print_backtrace ();
	$stack = ob_get_contents ();
	ob_end_clean ();
	if (defined ('FILESYSTEM_ABSOLUTEPATH_WEBSITE')) {
		$stack = str_replace (FILESYSTEM_ABSOLUTEPATH_WEBSITE, '', $stack);
	}
	$appel = [
		'conditions' => $conditions,
		'options' => $options,
		'stack' => "\n" . $stack
	];
	if (($action === 'del') and CACHELAB_EVITER_DOUBLON_DEL) {
		foreach ($appels_meme_hit[$action] ?? [] as $prec_appel) {
			if (($appel['conditions'] == $prec_appel['conditions'])
				and ($appel['options'] == $prec_appel['options'])) {
				cachelab_log ("NOUVEL APPEL ÉVITÉ DANS LE MÊME HIT pour $action - on ne recommence pas : " . print_r ($appel, 1) 
					."\nLes précédents étaient : ".print_r($appels_meme_hit[$action], 1)."\n====\n====\n====",
					"cachelab_evite_{$action}_meme_hit");
				return ['diag' => 'meme_hit'];
			}
		}
	}
	if ($appels_meme_hit[$action] ?? false) {
		cachelab_log (
			"Nouvel appel $action avec ".print_r($appel, 1)
				."\nLes précédents étaient : ".print_r($appels_meme_hit[$action], 1)."\n====\n====\n====", 
			"cachelab_{$action}_meme_hit");
	}
	$appels_meme_hit[$action][] = $appel;
	
	$n_cache = 0;
	$time = time ();
	foreach ($cache['cache_list'] as $d) {
		$n_cache++;
		// on "continue=passe au suivant" dés qu'on sait que le cache n'est pas cible
		$cle = $d['info'];
		$mdata=null;            // donnée mémoization

		// on passe les caches non concernés car d'autres origines
		// (et les caches d'un autre _CACHE_NAMESPACE pour ce même site)
		if (strpos($cle, _CACHE_NAMESPACE) !== 0) {
			$stats['nb_alien']++;
			continue;
		}

		// Effacer ou sauter les caches invalidés par une invalidation totale
		// ou que apcu ne suit plus
		if (
			(!apcu_exists($cle))                                            // cache apcu périmé
			or ($meta_derniere_modif > $d['creation_time'])                 // invalidation spip
			or ($d['ttl'] and ($d['creation_time'] + $d['ttl'] <= $time))   // cache APCU périmé
		) {
			if ($do_clean) {
				$memoiz_cle = substr($cle, $len_prefix);
				$ok_del = false;
				
				if (LOG_CACHELAB_CLEAN) {
					$why = ((!apcu_exists($cle)) ? '!apcu_exists ; ' : '')
						. (($meta_derniere_modif > $d['creation_time']) ? "spip meta_dern_modif=$meta_derniere_modif > crea=".$d['creation_time'].' ; ' : '')
						. (($d['creation_time'] + $d['ttl'] <= $time) ? 'apcu crea='.$d['creation_time'].' + ttl='.$d['ttl']." <= now=$time" : '');
					cachelab_log("CLEAN pour cibler($action...) : del $memoiz_cle $why", 'cachelab_clean');
				}
				// Avant ce test il arrivait parfois des salves de 10 à 50 logs d'échec du clean cache simultanés (mm t, mm pid)
				if ($Memoization->exists($memoiz_cle)) {
					$ok_del = $Memoization->del($memoiz_cle);
				}
				if (!$ok_del) {
					$ok_del = apcu_delete($cle);        // utile car les caches périmés sont encore là mais apcu_exists renvoie false 
				}
				if ($ok_del) {
					$stats['nb_clean']++;
				}
			}
			continue;
		}
		
		// à part pour cleaner, on ne cible que les caches de squelettes SPIP
		if (substr($cle, $len_prefix-1, 7) !== ':cache:') {
			continue;
		}

		// Il reste les caches SPIP véritablement candidats
		$stats['nb_candidats']++;

		$n_condition=0;
		$cible = false;
		// La premiere condition élémentaire composée entièrement satisfaite
		foreach ($l_conditions as $i_conditions) {
			$n_condition++;
			$c_session = $i_conditions['session'];
			$c_chemin = $i_conditions['chemin'];
			$c_cle_objet = $i_conditions['cle_objet'];
			$c_id_objet = $i_conditions['id_objet'];
			$c_plusfunc = $i_conditions['plusfunc'];
			
			if (($n_cache===1) and (_request('exec') === 'xray') and (_request('debug') === 'cachelab')) {
				echo "<b>Condition $n_condition</b> : chemin=$c_chemin, plusfunc=$c_plusfunc<br><xmp>".print_r($i_conditions,1)."</xmp>";
			}
			// 1er filtrage : par la session
			if ($c_session and (substr ($cle, -9) !== ('_' . $c_session))) {
				continue;
				// sur chaque échec on passe à la condition suivante dans le cas d'un OU ou d'un OR
			}

			// 2eme filtrage : par le chemin. 
			if ($c_chemin) {
				// tester la partie chemin de la clé précisément
				$parties = explode(':', $cle);
				$partie_cle = substr(end($parties), 33);                                // Enlever le préfixe hexa 				 
				$partie_cle = preg_replace(CACHELAB_PATTERN_SESSION, '', $partie_cle);  // Enlever la session (yc anonyme) si présente
				// debug_mode_trace('key', "partie_cle $partie_cle");
				// $org_log = $partie_cle;
				switch ($c_partie_chemin) {
					case 'tout':
					case 'chemin':
						break;
					case 'fichier':
						$parties = explode ('/', $partie_cle);
						$partie_cle = array_pop ($parties);
						break;
					case 'dossier':
						$parties = explode ('/', $partie_cle);
						array_pop ($parties);
						$partie_cle = implode ('/', $parties);
						break;
					default:
						cachelab_log ("Option partie_chemin incorrecte : '$c_partie_chemin'", 'ERREUR_cachelab');
						return null;
				}
				// debug_mode_trace('key', $org_log != $partie_cle ? 'PUIS '.$partie_cle : '');

				// Mémo php : « continue resumes execution just before the closing curly bracket },
				//              and break resumes execution just after the closing curly bracket } »
				switch ($c_methode_chemin) {
					case 'strpos':
						foreach (explode('|', $c_chemin) as $unchemin) {
							if (strpos ($partie_cle, $unchemin) !== false) {
								break 2;    // trouvé : sortir du foreach(chemin) ET du switch(methode) pour tester les autres conditions
							}
						}
						continue 2; // chemin non matché : sortir du switch et passer à la prochaine $i_condition du OU global
					case '==' :
					case 'egal' :
					case 'equal':
						foreach (explode('|', $c_chemin) as $unchemin) {
							if ($unchemin === $partie_cle) {
								break 2;    // trouvé : on sort du foreach ET du switch(methode) pour tester les autres conditions du ET
							}
						}
						continue 2;     // échec : passe à la $cle suivante
					case 'regexp':
						if (preg_match (",$c_chemin,i", $partie_cle)) {
							break;    // trouvé : poursuit le test des autres conditions du ET
						}
						continue 2;    // échec : passe à la clé suivante
					default:
						cachelab_log ("Méthode '$c_methode_chemin' pas prévue pour le filtrage par le chemin ".print_r([$action, $conditions, $options], 1), 'ERREUR_cachelab'._LOG_ERREUR);
						return null;
				}
			}

			// pour les filtres suivants on a besoin du contenu du cache
			if ($c_cle_objet or $c_plusfunc) {
				$mdata = $Memoization->get(substr ($cle, $len_prefix));
				if (!$mdata or !is_array ($mdata)) {
					cachelab_log ("clé=$cle : mdata est vide ou n'est pas un tableau : " . print_r ($mdata, 1), 'ERREUR_cachelab'._LOG_ERREUR);
					continue;
				}

				// 3eme filtre : par une variable du contexte de l'inclusion
				// On peut spécifier une valeur ou un ensemble de valeurs cibles, séparées par des | comme pour le chemin
				if ($c_cle_objet) {
					if (!isset($mdata['contexte'][$c_cle_objet])) {
						continue;   // échec, on regarde si ya une autre condition possible
					}
					// La clé est dans le contexte
					$contexte_val = $mdata['contexte'][$c_cle_objet];
					if (is_string ($c_id_objet) 
						and (strpos($c_id_objet, '|') !== false)            // liste de valeurs possibles pour la clé
						and !in_array($contexte_val, explode('|', $c_id_objet))) { // TODO Optimiser en préparant hors boucle
						continue;
					}
					if ((!is_string($c_id_objet) or (strpos($c_id_objet, '|') === false))
						and ($contexte_val != $c_id_objet)) {   // ATTENTION pas !== ici
						continue;
					}
				}

				// 4eme filtre : par une extension = une fonction spécifique à un site
				if ($c_plusfunc
					and !$c_plusfunc($action, $i_conditions, $options, $cle, $mdata, $stats)) {
					continue;
				}
			}
			if (LOG_CACHELAB_CONDITIONS
				and (!LOG_CACHELAB_CONDITIONS_EXCEPTION
					or  (($i_conditions['chemin'] ?? '') !== LOG_CACHELAB_CONDITIONS_EXCEPTION))) {
				cachelab_log ("Pour condition $n_condition, trouvé : $cle", "cachelab_cibler_cond_{$n_condition}_DEBUG");
			}
			// On sort de la boucle dès qu'une des conditions composée dans le OR global est satisfaite
			$cible = true;
			break;
		}
		if (!$cible) {
			// si on est sorti sans avoir trouvé on passe au cache suivant
			continue;
		}
		// restent les cibles atteintes
		$stats['nb_cible']++;
		if ($do_lists) {
			$stats['l_cible'][] = $cle;
		}

		if ((_request('exec') === 'xray') and (_request('debug') === 'cachelab')) {
			echo "Avec condition $n_condition trouvé $cle, nb_cible devient ".$stats['nb_cible']."<br>";
		}

		cachelab_appliquer($action, $cle, $mdata, $options, $return);

		if ($action === 'get') {
			return $return; 
		}
		// Possiblement ajouter option pour renvoyer stats aussi
		if (strpos ($action, 'get_') === 0) {
			return [$action => $return];
		}
	}

	$stats['chrono'] = microtime_do('end', 'ms');
	$msg = "cachelab_cibler($action) en {$stats['chrono']} ({$stats['nb_cible']} caches sur {$stats['nb_candidats']})"
		."\n".print_r($conditions, 1);
	if (count($options)) {
		$msg .= "\noptions = ".print_r($options, 1);
	}
	if (defined('LOG_CACHELAB_CHRONO') and LOG_CACHELAB_CHRONO) {
		if (function_exists ('debug_log')) {
			debug_log ($msg, 'cachelab_chrono');
		}
		else {
			cachelab_log ($msg, 'cachelab_chrono.'._LOG_INFO);
		}
	}
	if (defined('LOG_CACHELAB_SLOW') and ($stats['chrono']  > LOG_CACHELAB_SLOW)) {
		if (function_exists ('debug_log')) {
			debug_log ($msg, 'cachelab_slow', true);
		}
		else {
			cachelab_log ($msg, 'cachelab_slow.'._LOG_INFO_IMPORTANTE);
		}
	}
	if (($action === 'del') and defined('LOG_CACHELAB_TOOMANY_DEL') and ($stats['nb_cible']  > LOG_CACHELAB_TOOMANY_DEL)) {
		if (function_exists ('debug_log')) {
			debug_log ($msg, 'cachelab_toomany_del', true);
		}
		else {
			cachelab_log($msg, 'cachelab_toomany_del.'._LOG_INFO_IMPORTANTE);
		}
	}

	if ($action === 'get' or (strpos ($action, 'get_') === 0)) {
		return $return;
	}
	return $stats;
}

/**
 * @param string $action    : 'stop', 'select' ou 'go'
 * @param array $objets_invalidants
 */
function controler_invalideur(string $action, array $objets_invalidants = array()) {
static $prev_derniere_modif_invalide;
	switch ($action) {
	case 'stop':
		$objets_invalidants = array();
		// nobreak;
	case 'select':
		$prev_derniere_modif_invalide = $GLOBALS['derniere_modif_invalide'];
		if (is_array($objets_invalidants)) {
			$GLOBALS['derniere_modif_invalide'] = $objets_invalidants;
		}
		break;
	case 'go':
		$GLOBALS['derniere_modif_invalide'] = $prev_derniere_modif_invalide;
		break;
	}
}

/**
 * @param string $action_unused
 * @param array $conditions
 * @param array $options_unused
 * @param string $cle_unused
 * @param array &$data
 * @param array &$stats
 * @return bool
 *
 * Exemple d'extension utilisable avec 'plus'=>'contexte'
 * Filtrer non sur une seule valeur de l'environnement comme avec 'cle_objet'
 * mais sur un ensemble de valeurs spécifié par $conditions['contexte']
 * qui est un tableau de (clé, valeur)
 * Toutes les valeurs doivent être vérifiées dans l'environnement.
 */
function cachelab_ciblercache_contexte(string $action_unused, array $conditions, array $options_unused, string $cle_unused, array &$data, array &$stats): bool {
	if (!isset($data['contexte'])) {
		return false;
	}
	if (!isset($conditions['plus_args']) or !is_array($conditions['plus_args'])) {
		// avec debug_assert(false, "cachelab_ciblercache_contexte sans contexte dans plus_args($action, conditions=".print_r($conditions,1));
		// la phpstack fait planter donc pas d'assert
		$m = "ERREUR cachelab_ciblercache_contexte sans contexte dans plus_args ($action_unused, conditions=<xmp>".print_r($conditions,1)."</xmp>";
		echo $m;
		cachelab_log($m, 'ERREUR_cachelab_contexte'._LOG_ERREUR);
		return false;
	}
	$diff = array_diff_assoc($conditions['plus_args'], $data['contexte']);
	return empty($diff);
}
