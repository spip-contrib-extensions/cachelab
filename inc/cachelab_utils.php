<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

const CACHELAB_PATTERN_SESSION = '/_([a-f0-9]{8}|)$/i'; // FIXME Doublonne 'XRAY_PATTERN_SESSION'

//
// sépare à la lisp le premier mot et les autres
//
// $f (string) : liste à séparer
// $cdr (string ou array) : valeur par défaut pour le 2eme élt renvoyé
// $string_wanted (bool) : type du 2eme élément renvoyé
//
// retour (array) :
// retour[0] (string) : 1er élément de la liste
// retour[1] (string ou array selon $string_wanted) : reste de la liste
//
/**
 * @param string $l             liste de termes séparés par des espaces
 * @param string $cdr           suite par défaut
 * @return array                renvoie un tableau [ car, cdr ]
 *      où car est le premier élément de la liste
 *      et cdr est le reste de la liste, ou le cdr reçu en argument s'il n'y a pas de reste
 *
 */
function split_first_arg(string $l, string $cdr = ''): array {
	$l = preg_replace('/\s+/', ' ', trim($l), -1, $n);
	$lparts = explode(' ', $l);
	if ($lparts != array_filter($lparts)) {
		spip_log("split_first_arg($l, $cdr); mauvais format du 1er argument".print_r($lparts, 1), 'cachelab_ASSERT'._LOG_ERREUR);
		exit;
	}
	$car = array_shift($lparts);
	if (!$car) {
		spip_log("split_first_arg($l,$cdr) : pb avec le 1er argument, reste lparts=".print_r($lparts, 1), 'cachelab_ASSERT'._LOG_ERREUR);
		exit;
	}

	if ($lparts) {
		$cdr = implode(' ', $lparts);
	}

	return array ($car, $cdr);
}

/**
 * @param string $chemin
 * @param string $sep
 * @return string   remplace les / par des _
 */
function slug_chemin(string $chemin, string $sep = '_'): string {
	return str_replace('/', $sep, $chemin);
}


/**
 * @param string $cond              signal d'invalidation
 *       typiquement de la forme : "id='document/1234'" ou "id='article/567'"
 *       ... mais parfois aussi de la forme "id='id_document/1234'" 
 * @param string $objet_attendu     id_objet attendu : 'id_document' ou 'id_article'
 * @param string &$objet            objet effectivement trouvé dans $cond : 'document' ou 'article' (ou 'id_document' actuellement si c'est ce qui est reçu hmmm...)
 * @return int|null
 *
 * Fenvoie l'id_objet ciblé par le signal, ou null en cas d'erreur
 * 
 * Fonction utilitaire pour les fonctions cachelab_suivre_invalideur_xxx user-squelette-définies
 * Exemple :
 * function cachelab_suivre_invalideur_document($cond, $modif) {
 *	   include_spip('inc/cachelab');
 *     $id_doc = decoder_invalideur($cond, 'document');
 *     cachelab_cibler('del', array ('chemin'=>'documents', 'cle_objet'=>'id_document', 'id_objet'=>$id_doc));
 *     return false;
 * }
 */
function decoder_invalideur($cond, $objet_attendu = '', &$objet='') {
	// On ne regarde que ce qui est entre '' ou ""
	if (!preg_match(',(["\'])([a-z_]+)/(\d+)\1,', $cond, $r)) {
		spip_log("Signal non conforme pour decode_signal_invalideur ($cond, $objet_attendu)", 'ERREUR_cachelab'._LOG_ERREUR);
		return null;
	}
	[ , , $objet, $id_objet] = $r;
	// On vérifie que le signal répond à notre attente
	if ($objet_attendu and ($objet!==$objet_attendu) and ($objet!=="id_$objet_attendu")) {
		spip_log(
			"decoder_invalideur($cond,) ne reçoit pas un '$objet_attendu' mais un '$objet' (avec id_objet='$id_objet'). Signal non conforme ?",
			'ERREUR_cachelab'._LOG_ERREUR
		);
		return null;
	}
	if (!(int)$id_objet) {
		spip_log(
			"decoder_invalideur($cond, $objet_attendu) ne reçoit pas l'id pour '$objet'",
			'ERREUR_cachelab'._LOG_ERREUR
		);
		return null;
	}
	return (int)$id_objet;
}

function cachelab_log(string $m, string $f, bool $s=false): string {
	if (function_exists ('debug_log')) {
		return debug_log ($m, $f, $s);
	}
	spip_log($m, $f._LOG_ERREUR);
	return $m;
}